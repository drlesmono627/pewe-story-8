from django.test import TestCase, Client, LiveServerTestCase
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time


# Create your tests here.
class EruditoTests(TestCase):
    def link_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def header_exist(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('id="header"', content)

    def search_box_exist(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('id="search_box"', content)

