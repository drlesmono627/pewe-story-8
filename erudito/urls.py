from django.contrib import admin
from django.urls import path
from . import views

app_name = 'erudito'

urlpatterns = [
	path('', views.erudito, name='searchbooks'),
]