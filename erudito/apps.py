from django.apps import AppConfig


class EruditoConfig(AppConfig):
    name = 'erudito'
